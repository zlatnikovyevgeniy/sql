package com.shpp.app.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class ProductGenerator {

    private static final Logger logger = LoggerFactory.getLogger(ProductGenerator.class);

    private static int getRandomNumberUsingNextInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public static ProductDTO generateProduct(){
        return new ProductDTO(generateName(),generateCategory());
    }

    public static String generateName() {
        int leftLimit = 97;
        int rightLimit = 122;
        int targetStringLength = getRandomNumberUsingNextInt(3, 20);
        logger.debug("Create 3 auxiliary variables to generate a string with targetStringLength " +
                "from the small letter a to the small letter z in ASCII table.");
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static int generateCategory(){
        return getRandomNumberUsingNextInt(1,6);
    }
}
