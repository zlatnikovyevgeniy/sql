package com.shpp.app.product;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.PositiveOrZero;

import java.util.StringJoiner;

public class ProductDTO {

    @PositiveOrZero
    long id;
    @Pattern(regexp = "[a-zA-Z]{3,20}")
    String name;
    @PositiveOrZero
    long categoryId;

    public ProductDTO() {
    }

    public ProductDTO(String name, long categoryId) {
        this.name = name;
        this.categoryId = categoryId;
    }

    public ProductDTO(long id, String name, long categoryId) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ProductDTO.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("categoryId=" + categoryId)
                .toString();
    }
}
