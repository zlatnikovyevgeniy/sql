package com.shpp.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.shpp.app.DBInteractor.TRANSACTION_SIZE;
import static com.shpp.app.Main.NUMBER_OF_PRODUCTS;
import static com.shpp.app.PropertyReader.readProperty;

public class MyThread extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(MyThread.class);
    private static final int NUMBER_OF_THREADS = 10;
    public static final String FILENAME = "connection.properties";
    public static final String JDBC_URL = readProperty(FILENAME).getProperty("url");
    public static final String USERNAME = readProperty(FILENAME).getProperty("username");
    public static final String PASSWORD = readProperty(FILENAME).getProperty("password");

    private static int messageCounter = 0;

    @Override
    public void run() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            logger.error("Can't connect to database!", e);
            throw new RuntimeException(e);
        }
        DBInteractor dbInteractor = new DBInteractor(connection);
        dbInteractor.insertIntoProductsTable();
        messageCounter += TRANSACTION_SIZE;
        logger.info("{} products was sent", messageCounter);
        System.gc();
    }

    public static void sendStreamOfProducts() {
        int counter = 0;
        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
        while (counter < NUMBER_OF_PRODUCTS) {
            executorService.execute(new MyThread());
            counter += TRANSACTION_SIZE;
        }
        executorService.shutdown();
        while (!executorService.isTerminated()) {}
    }
}
