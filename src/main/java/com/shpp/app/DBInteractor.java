package com.shpp.app;

import com.shpp.app.product.ProductDTO;
import com.shpp.app.product.ProductGenerator;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class DBInteractor {
    private static final Logger logger = LoggerFactory.getLogger(DBInteractor.class);
    public static final int TRANSACTION_SIZE = Integer.parseInt(PropertyReader
            .readProperty("config.properties")
            .getProperty("trn_size"));
    private Connection connection;
    private ValidatorFactory factory;
    private Validator validator;
    private Set<ConstraintViolation<ProductDTO>> violations;
    public static final CSVWriter csvWriter = new CSVWriter();

    public DBInteractor(Connection connection){
        this.connection = connection;
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            logger.warn("Can't set autocommit to false", e);
        }
        factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public void insertIntoCategoriesTable() {
        try {
            String sql = "INSERT INTO categories(name) VALUES (?)";

            PreparedStatement statement = connection.prepareStatement(sql);


            statement.setString(1, "clothes");
            statement.addBatch();
            statement.executeBatch();

            connection.commit();
            connection.close();
        } catch (SQLException ex) {
            logger.warn("Transaction failed!", ex);

            try {
                connection.rollback();
            } catch (SQLException e) {
                logger.error("Rollback failed!", e);
                throw new RuntimeException(e);
            }
        }
    }

    public void insertIntoProductsTable() {
        try {
            String sql = "INSERT INTO products(name,category_id) VALUES (?,?)";

            PreparedStatement statement = connection.prepareStatement(sql);

            Stream.generate(ProductGenerator::generateProduct)
                    .limit(TRANSACTION_SIZE)
                    .forEach(productDTO -> {
                        violations = validator.validate(productDTO);
                        if (violations.isEmpty()) {
                            try {
                                statement.setString(1, productDTO.getName());
                                statement.setLong(2, productDTO.getCategoryId());
                                statement.addBatch();
                            } catch (SQLException e) {
                                logger.error("Can't add value to prepared statement!", e);
                                throw new RuntimeException(e);
                            }
                        }else {
                            List<String> list = new ArrayList<>();
                            for (ConstraintViolation<ProductDTO> v : violations) {
                                list.add(v.getMessage());
                            }
                            csvWriter.renderInvalidMessages(productDTO,list);
                        }
                    });
            statement.executeBatch();

            connection.commit();
            connection.close();
        } catch (SQLException ex) {
            logger.warn("Transaction failed!", ex);

            try {
                connection.rollback();
            } catch (SQLException e) {
                logger.error("Rollback failed!", e);
                throw new RuntimeException(e);
            }
        }
    }

    public void insertIntoShopsTable() {
        try {
            String sql = "INSERT INTO shops(address) VALUES (?)";

            PreparedStatement statement = connection.prepareStatement(sql);


            statement.setString(1, "Stepana Bandery 36-A");
            statement.addBatch();
            statement.executeBatch();

            connection.commit();
            connection.close();
        } catch (SQLException ex) {
            logger.warn("Transaction failed!", ex);

            try {
                connection.rollback();
            } catch (SQLException e) {
                logger.error("Rollback failed!", e);
                throw new RuntimeException(e);
            }
        }
    }

    public void insertIntoStorageTable() {
        try {
            String sql = "INSERT INTO storage(quantity,shop_id,product_id) VALUES (?,?,?)";

            PreparedStatement statement = connection.prepareStatement(sql);


            statement.setInt(1, 1000);
            statement.setInt(2, 2);
            statement.setInt(3, 11435);
            statement.addBatch();
            statement.executeBatch();

            connection.commit();
            connection.close();
        } catch (SQLException ex) {
            logger.warn("Transaction failed!", ex);

            try {
                connection.rollback();
            } catch (SQLException e) {
                logger.error("Rollback failed!", e);
                throw new RuntimeException(e);
            }
        }
    }

    public void selectFromTable() {
        try {
            String sql = "SELECT c.name, sh.address, st.quantity FROM categories c " +
                    "JOIN products p ON c.id=p.category_id " +
                    "JOIN storage st ON p.id=st.product_id " +
                    "JOIN shops sh ON sh.id=st.shop_id " +
                    "WHERE st.quantity=(" +
                    "SELECT MAX(quantity) FROM storage st " +
                    "JOIN products p ON p.id=st.product_id " +
                    "JOIN categories c ON c.id=p.category_id " +
                    "WHERE c.name=?)";

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, System.getProperty("type", "dishes"));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                logger.info("{} - {} - {}", resultSet.getString("name"),
                        resultSet.getString("address"), resultSet.getString("quantity"));
            }

            connection.commit();
            connection.close();
        } catch (SQLException ex) {
            logger.warn("Transaction failed!", ex);

            try {
                connection.rollback();
            } catch (SQLException e) {
                logger.error("Rollback failed!", e);
                throw new RuntimeException(e);
            }
        }
    }
}
