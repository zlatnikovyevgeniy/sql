package com.shpp.app;


import com.shpp.app.product.ProductDTO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


public class CSVWriter {

    private static final Logger logger = LoggerFactory.getLogger(CSVWriter.class);

    private CSVPrinter csvPrinter;

    CSVWriter() {
        try {
            csvPrinter = new CSVPrinter(Files.newBufferedWriter(Paths.get("invalid.csv")),
                    CSVFormat.DEFAULT.withHeader("name", "category_id","errors"));
        } catch (IOException e) {
            logger.error("Can't create a csv file!", e);
            throw new RuntimeException("IOException arose while creating csv file!", e);
        }
    }

    public void renderInvalidMessages(ProductDTO productDTO,List errors) {
        String err = String.join(",", errors);
        try {
            csvPrinter.printRecord(productDTO.getName(), productDTO.getCategoryId(), err);
        } catch (IOException e) {
            logger.error("Can't write message {} to the csv file!",productDTO, e);
        }
    }


    public void closeFile() {
        try {
            csvPrinter.flush();
            csvPrinter.close();
        } catch (IOException e) {
            logger.error("Can't close csv files!", e);
            throw new RuntimeException("IOException arose while closing csv files!", e);
        }
    }
}
