package com.shpp.app;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;


import static com.shpp.app.DBInteractor.csvWriter;
import static com.shpp.app.MyThread.*;


public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    public static final int NUMBER_OF_PRODUCTS = Integer.parseInt(PropertyReader
            .readProperty("config.properties")
            .getProperty("num_of_messages"));

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        sendStreamOfProducts();
        stopWatch.stop();
        long time = stopWatch.getTime(TimeUnit.SECONDS);
        logger.info("Time: {}. WPS = {}", time, (double) NUMBER_OF_PRODUCTS / time);
        csvWriter.closeFile();

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            logger.error("Can't connect to database!",e);
            throw new RuntimeException(e);
        }
        DBInteractor dbInteractor = new DBInteractor(connection);
        dbInteractor.selectFromTable();
    }
}