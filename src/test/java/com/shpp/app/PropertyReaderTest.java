package com.shpp.app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static com.shpp.app.PropertyReader.readProperty;

class PropertyReaderTest {

    private static final String FILE_NAME = "connection.properties";

    @ParameterizedTest
    @MethodSource("provideValuesForReadProperty")
    void readPropertyTest(String value, String propertyName) {
        Assertions.assertEquals(value, readProperty(FILE_NAME).getProperty(propertyName));
    }

    private static Stream<Arguments> provideValuesForReadProperty() {
        return Stream.of(
                Arguments.of("jdbc:postgresql:reference", "url"),
                Arguments.of("myName", "username"),
                Arguments.of("myPassword", "password")
        );
    }

    @Test
    void readPropertyNegativeTest() {
        Assertions.assertThrows(RuntimeException.class, () -> readProperty(null));
    }
}