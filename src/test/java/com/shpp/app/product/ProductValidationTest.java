package com.shpp.app.product;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductValidationTest {
    private static final Logger logger = LoggerFactory.getLogger(ProductValidationTest.class);

    @ParameterizedTest
    @MethodSource("provideProductsForTest")
    void fieldValidationTest(int messageQuantity, ProductDTO productDTO){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<ProductDTO>> violations = validator.validate(productDTO);
        for(ConstraintViolation<ProductDTO> v:violations) logger.info(v.getMessage());
        assertEquals(messageQuantity, violations.size());
    }

    private static Stream<Arguments> provideProductsForTest() {
        return Stream.of(
                Arguments.of(0,new ProductDTO(1,"qwrtyy",1)),
                Arguments.of(1,new ProductDTO(-1,"abyetrumy",1)),
                Arguments.of(1,new ProductDTO(1,"ryneuiutnnn",-1)),
                Arguments.of(1,new ProductDTO(1,"",1)),
                Arguments.of(3,new ProductDTO(-10,"qwertyuiopasdfghjklzxc",-10))

        );
    }
}
