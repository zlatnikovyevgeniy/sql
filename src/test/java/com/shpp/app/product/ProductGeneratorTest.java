package com.shpp.app.product;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ProductGeneratorTest {

    @Test
    void generateProductTest() {
        Assertions.assertDoesNotThrow(ProductGenerator::generateProduct);
    }

    @Test
    void generateNameTest() {
        Assertions.assertDoesNotThrow(ProductGenerator::generateName);
    }

    @Test
    void generateCategoryTest() {
        Assertions.assertDoesNotThrow(ProductGenerator::generateCategory);
    }
}