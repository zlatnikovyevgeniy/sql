package com.shpp.app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;

import static com.shpp.app.DBInteractor.TRANSACTION_SIZE;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(MockitoExtension.class)
class DBInteractorTest {

    @InjectMocks
    private static DBInteractor dbInteractor;

    @Mock
    private static Connection connection;

    @Mock
    private static PreparedStatement statement;

    @Mock
    private static ResultSet resultSet;

    @BeforeAll
    static void setUp() throws SQLException {
        connection = Mockito.mock(Connection.class);
        Mockito.doNothing().when(connection).setAutoCommit(false);
        dbInteractor = new DBInteractor(connection);
    }

    @Test
    void insertIntoCategoriesTableTest() throws SQLException {
        Mockito.when(connection.prepareStatement(anyString())).thenReturn(statement);
        ArgumentCaptor<Integer> arg = ArgumentCaptor.forClass(Integer.class);
        dbInteractor.insertIntoCategoriesTable();

        Mockito.verify(statement,Mockito.times(1))
                .setString(arg.capture(),anyString());
        Assertions.assertEquals(1,arg.getValue());

        Mockito.verify(statement,Mockito.times(1))
                .addBatch();
        Mockito.verify(statement,Mockito.times(1))
                .executeBatch();

        Mockito.verify(connection,Mockito.times(1))
                .commit();
        Mockito.verify(connection,Mockito.times(1))
                .close();
    }

    @Test
    void insertIntoCategoriesTableNegativeTest() throws SQLException {
        Mockito.when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);
        dbInteractor.insertIntoCategoriesTable();
        Mockito.verify(connection,Mockito.times(1))
                .rollback();
    }

    @Test
    void insertIntoProductsTableTest() throws SQLException {
        Mockito.when(connection.prepareStatement(anyString())).thenReturn(statement);
        ArgumentCaptor<Integer> arg = ArgumentCaptor.forClass(Integer.class);
        dbInteractor.insertIntoProductsTable();

        Mockito.verify(statement,Mockito.atLeast(1))
                .setString(arg.capture(),anyString());
        Assertions.assertEquals(1,arg.getValue());

        Mockito.verify(statement,Mockito.atLeast(1))
                .setLong(arg.capture(),anyLong());
        Assertions.assertEquals(2,arg.getValue());

        Mockito.verify(statement,Mockito.atLeast(1))
                .addBatch();
        Mockito.verify(statement,Mockito.times(1))
                .executeBatch();

        Mockito.verify(connection,Mockito.times(1))
                .commit();
        Mockito.verify(connection,Mockito.times(1))
                .close();
    }

    @Test
    void insertIntoProductsTableNegativeTest() throws SQLException {
        Mockito.when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);
        dbInteractor.insertIntoProductsTable();
        Mockito.verify(connection,Mockito.times(1))
                .rollback();
    }

    @Test
    void insertIntoShopsTable() throws SQLException {
        Mockito.when(connection.prepareStatement(anyString())).thenReturn(statement);
        ArgumentCaptor<Integer> arg = ArgumentCaptor.forClass(Integer.class);
        dbInteractor.insertIntoShopsTable();

        Mockito.verify(statement,Mockito.times(1))
                .setString(arg.capture(),anyString());
        Assertions.assertEquals(1,arg.getValue());

        Mockito.verify(statement,Mockito.times(1))
                .addBatch();
        Mockito.verify(statement,Mockito.times(1))
                .executeBatch();

        Mockito.verify(connection,Mockito.times(1))
                .commit();
        Mockito.verify(connection,Mockito.times(1))
                .close();
    }

    @Test
    void insertIntoShopsTableNegativeTest() throws SQLException {
        Mockito.when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);
        dbInteractor.insertIntoShopsTable();
        Mockito.verify(connection,Mockito.times(1))
                .rollback();
    }

    @Test
    void insertIntoStorageTable() throws SQLException {
        Mockito.when(connection.prepareStatement(anyString())).thenReturn(statement);
        ArgumentCaptor<Integer> arg = ArgumentCaptor.forClass(Integer.class);
        dbInteractor.insertIntoStorageTable();

        Mockito.verify(statement,Mockito.times(3))
                .setInt(arg.capture(),anyInt());
        Assertions.assertEquals(3,arg.getValue());

        Mockito.verify(statement,Mockito.times(1))
                .addBatch();
        Mockito.verify(statement,Mockito.times(1))
                .executeBatch();

        Mockito.verify(connection,Mockito.times(1))
                .commit();
        Mockito.verify(connection,Mockito.times(1))
                .close();
    }

    @Test
    void insertIntoStorageTableNegativeTest() throws SQLException {
        Mockito.when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);
        dbInteractor.insertIntoStorageTable();
        Mockito.verify(connection,Mockito.times(1))
                .rollback();
    }

    @Test
    void selectFromTable() throws SQLException {
        Mockito.when(connection.prepareStatement(anyString())).thenReturn(statement);
        Mockito.when(statement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true)
                .thenReturn(false);
        Mockito.when(resultSet.getString(anyString()))
                .thenReturn("name")
                .thenReturn("address")
                .thenReturn("quantity");
        ArgumentCaptor<String> arg = ArgumentCaptor.forClass(String.class);
        dbInteractor.selectFromTable();

        Mockito.verify(resultSet,Mockito.times(3)).getString(arg.capture());
        Assertions.assertEquals("quantity",arg.getValue());

    }
}