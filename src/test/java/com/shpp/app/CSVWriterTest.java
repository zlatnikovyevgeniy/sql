package com.shpp.app;

import com.shpp.app.product.ProductDTO;
import org.apache.commons.csv.CSVPrinter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;

class CSVWriterTest {

    private static CSVWriter csvWriter;
    private static CSVPrinter csvPrinter;

    @BeforeAll
    static void setUp() throws NoSuchFieldException, IllegalAccessException {
        csvWriter = new CSVWriter();
        csvPrinter = Mockito.mock(CSVPrinter.class);
        Field field1 = csvWriter.getClass().getDeclaredField("csvPrinter");
        field1.setAccessible(true);
        field1.set(csvWriter, csvPrinter);

    }

    @Test
    void renderInvalidMessagesTest() throws IOException {
        String name = "product1";
        long category = 1;
        ProductDTO productDTO = new ProductDTO(name, category);
        List<String> list = Arrays.asList("error1", "error2");
        String err = String.join(",", list);

        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Long> categoryCaptor = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<String> errorsCaptor = ArgumentCaptor.forClass(String.class);
        csvWriter.renderInvalidMessages(productDTO, list);

        Mockito.verify(csvPrinter, Mockito.times(1))
                .printRecord(nameCaptor.capture(), categoryCaptor.capture(), errorsCaptor.capture());
        Assertions.assertEquals(name, nameCaptor.getValue());
        Assertions.assertEquals(category, categoryCaptor.getValue());
        Assertions.assertEquals(err, errorsCaptor.getValue());
    }

    @Test
    void closeFileTest() throws IOException {
        csvWriter.closeFile();
        Mockito.verify(csvPrinter, Mockito.times(1)).flush();
        Mockito.verify(csvPrinter, Mockito.times(1)).close();
    }
}